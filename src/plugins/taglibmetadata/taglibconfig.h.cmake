/* taglibconfig.h.  Generated by cmake from taglibconfig.h.cmake */
#pragma once

/* Define if you have TagLib installed */
#cmakedefine HAVE_TAGLIB_ID3V23_SUPPORT 1
#cmakedefine HAVE_TAGLIB_XM_SUPPORT 1
